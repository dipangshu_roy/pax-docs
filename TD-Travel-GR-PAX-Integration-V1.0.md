
**TD Travel GR & PAX Integration**

This document describes the details regarding the integration of the existing TD GR system and the Vernost system. The current PAX details/schema which is provided by the Vernost is not adequate to cater to the vison of the TD platform, and its proposed that TD created an extended schema at the TD platform side to cater to capture all the relevant travel specific extension to the existing GR record.

In travel booking content, there are two entities involved 

1. The Booker, who does the actual booking transaction, payment and check out 

2. The PAX, who is the actual person for which the booking is done (i.e. the traveling person). 

The system level login & transaction details will be registered in the name of booker (Customer), while all the loyalty benefits are computed and provided to the actual PAX who travels.

If the customer also becomes a traveler, then a corresponding PAX record (with all travel extension attributes) need to be created for the customer GR record. Only the PAX records will be made part of any booking records and the associated PAX information details.

In order to facilitate this travel domain specific extension schema/attributes, its proposed that a new micros service will be created in the TD Platform side which will provide the necessary API support and data management capabilities associated with the PAX while extending on the existing GR record capabilities. It is envisaged to avoid duplication of attributes which are in GR in the PAX schema, however due the specific business flow requirements there may instances where the PAX schema may duplicate certain attributes which are already available in the GR record. If such duplication happens, then the context will decide which attributes to consider, i.e. in a PAX info content, it would be the PAX attributes which will take precedence, while in the GR info context, it would be the GR attributes which will take precedence. 

**Proposed Solution Approach**

1. PAX records exist only in the context of a Customer(Booker) GR record. Ie for a PAX record to be added to the booking transactional record, the PAX record need to be first created in the GR PAX record table, and obtain the hash/ID. All GR PAX record will always carry the customer hash (GR Hash) reference to which the PAX record is associated with. A relation of the PAX to the customer will also be stored as part of the PAX record.

    A new micro service and APIs end points will be created for this (refer to **Travel PAX Information Service**)

2. The PAX record can be associated with none, one or more transaction/booking records

3. PAX records will be created and maintained in the TD GR eco system. The PAX IDs/Hash is owned and managed by TD GR PAX system.

4. Same PAX (in content terms) can exist with multiple Customers, however PAX records will be different for each customer.

5. At booking/transactional level, a minimal set of fields (10 to 12) of PAX records will be copied / duplicated at the Vernost platform side as part of the transaction (to maintain conformity to existing PAX info details related to booking transaction info). This is required to manage the business flows at vernost system level.
All such transactional record will also store the GR PAX record hash/ID for cross reference to GR Side for extended attributes stored in GR PAX record.
All other extended attributes of PAX data will be always stored and maintained as part of the GR PAX data table & service. Any access/updates to this part of the PAX should be carried out thru the PAX Service & API end points.

6. Any changes / updates / edits carried out to the PAX records at GR side will not impact any PAX info in existing transactional records (ie copied over PAX data). However any new transactions/bookings carried out will take the new updated PAX data.

7. If any changes to be carried out to the transactional level/booking details PAX data, the same need to go thru a booking amendment business process.

**Architecture Approach**

This section describes the overall architecture approach for achieving the proposed integration.

**ER Diagram**

![ER Diagram](images/ER_Diagram.png)

Note : Each entities which are represented in the above diagram will have its own detailed attributes scheme of their own. The detailed regarding these attributes are limited to the purpose and scope of this integration.


**Service Integration Architecture**

![Service Integration Architecture](images/Service_Integration_Architecture.png)


**Data flow Diagram**

High Level DFD

![High Level DFD](images/High_Level_DFD.png)

**Detailed DFD – Create Passenger**

![Create Passenger DFD](images/Create_Passenger_DFD.png)

**Integration Call flow**

![Integration Call flow](images/Integration_Call_Flow.png)

Note : to be corrected based on the vernost actual internal call flows for managing PAX records


**Travel PAX Information Service**

This will be a new micro service. The details of the APIs and the associated attributes etc are provided below (This needs to be expanded to reflect all the service capabilities which are offered by this service).

API Documentation URL:

<https://tdldev.stoplight.io/studio/travel-pax-profile-service?source=x1tqglvr&symbol=%252Fp%252Freference%252FProfile_PAX_API.json%252Fpaths%252F%7E1pax%7E1%257Bpax_id%257D%7E1info%252Fget>

**API Name :** Create PAX

**Method:** POST

**URL** : <https://dapi.tatadigital.com/api/v1/travel/pax>

**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Request JSON**	

**JSON containing the detailed PAX data which need to be used to create the PAX record**

```json
{
    "customer_gr_hash": "strr9123",
    "relation": "strr9123",
    "externalReferenceNumber": "stt90123"
} 

```

**API Name :** Get PAX Details

**Method:** GET

**URL** : <https://dapi.tatadigital.com/api/v1/travel/pax/pax_id>

**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Response JSON**

**API Name :** Update PAX Details

**Method:** PUT

**URL** : <https://dapi.tatadigital.com/api/v1/travel/pax/pax_id>


**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Response JSON**

**API Name :** Delete PAX Details

**Method:** POST

**URL** : <https://dapi.tatadigital.com/api/v1/travel/pax>/pax\_id

**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Response JSON**

**API Name :** Update PAX Record fields	

**Method:** PATCH	

**URL:** <https://dapi.tatadigital.com/api/v1/travel/pax/{pax_id}/{field_name}>

**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Response JSON**

**API Name:** Get PAX basic info	

**Method:** GET	

**URL:** [https://dapi.tatadigital.com/api/v1/travel/pax/{pax_id}/info](https://dapi.tatadigital.com/api/v1/travel/pax/{pax_id}/info)

**Description:** 

Returns the basic info such as PAX names for {pax\_id}. Required for PAX selection while booking

**Request Header :**

client\_id : <client\_id>

authorization : <sso\_token>

store\_id : <store\_id>

**Response JSON**

**API Name:** Get PAX basic info	

**Method:** GET	

**URL:** https://dapi.tatadigital.com/api/v1/travel/pax/{pax_id}/info

**Description:**

Returns the basic info such as PAX names for {pax_id}. Required for PAX selection while booking

**Error Handling & Codes**

The error handling & codes will be updated after we have gone through the existing services.

**PAX Schema Definition**

Details the travel specific extended attributes associated with the customer/PAX records

**AIR -  PASSENGER_DETAIL**

| **Field Name**   | **Data Type** | **NUL** | **Description**                                                               | **Available in GR Profile** | **Available in GR Update API** |
| :--------------- | :------------ | :------ | :---------------------------------------------------------------------------- | :-------------------------- | :----------------------------- |
| BOOKER_MEMBER_ID | varchar(15)   | N       | Indicates First Name of the passenger                                         | N/A                         | N/A                            |
| TITLE            | varchar(5)    | N       | Indicates Title of the passenger                                              | Yes                         | Yes                            |
| FIRST_NAME       | varchar(80)   | N       | Indicates First Name of the passenger                                         | Yes                         | Yes                            |
| MIDDLE_NAME      | varchar(80)   | Y       | Indicates Middle Name of the passenger                                        | Yes                         | Yes                            |
| LAST_NAME        | varchar(80)   | N       | Indicates Last Name of the passenger                                          | Yes                         | Yes                            |
| DOB              | varchar(20)   | Y       | Indicates Date of Birth of the passenger                                      | Yes                         | Yes                            |
| PAX_EMAIL_ID     | varchar(80)   | N       | Indicates Email ID of the passenger                                           | Yes                         | Yes                            |
| PAX_MOBILE_NO    | varchar(20)   | N       | Indicates Mobile Number of the passenger                                      | Yes                         | No                             |
| PASSPORT_NO      | varchar(30)   | Y       | Indicates Passport Number of the passenger for international journey          | No                          | Yes                            |
| PASSPORT_ISSUE   | varchar(30)   | Y       | Indicates Passport issuing Country of the passenger for international journey | No                          | Yes                            |
| PASSPORT_EXPIRY  | varchar(30)   | Y       | Indicates Passport Expiry Date of the passenger for international journey     | No                          | Yes                            |


**AIR -  FLT\_TXN\_PASSENGER\_DETAIL**

| **Field Name**        | **Data Type** | **NUL** | **Description**                                                                                          |
| :-------------------- | :------------ | :------ | :------------------------------------------------------------------------------------------------------- |
| PASSENGER\_DETAIL\_ID | int(11)       | N       | Primary Key of the table                                                                                 |
| FB\_BOOKING\_REF\_NO  | int(11)       | N       | Hold the reference ID for the order created. This would be reference to the table flt\_txn\_flight\_book |
| PASSENGER\_NO         | int(2)        | N       | Indicates Passenger Number for the opted journey                                                         |
| TITLE                 | varchar(5)    | N       | Indicates Title of the passenger                                                                         |
| FIRST\_NAME           | varchar(80)   | N       | Indicates First Name of the passenger                                                                    |
| MIDDLE\_NAME          | varchar(80)   | Y       | Indicates Middle Name of the passenger                                                                   |
| LAST\_NAME            | varchar(80)   | N       | Indicates Last Name of the passenger                                                                     |
| GENDER                | int(2)        | Y       | Indicates gender of the passenger                                                                        |
| PASSENGER\_TYPE       | varchar(3)    | N       | Indicates the type of Passenger                                                                          |
| DOB                   | varchar(20)   | Y       | Indicates Date of Birth of the Passenger                                                                 |
| PAX\_EMAIL\_ID        | varchar(80)   | N       | Indicates Email ID of the Passenger                                                                      |
| PAX\_MOBILE\_NO       | varchar(20)   | N       | Indicates Mobile Number of the Passenger                                                                 |
| PASSPORT\_NO          | varchar(30)   | Y       | Indicates Passport Number of the Passenger for International Journey                                     |
| BOOKER\_EMAIL\_ID     | varchar(80)   | Y       | Indicates registered Email ID for redeem journey                                                         |
| BOOKER\_MOBILE\_NO    | varchar(20)   | Y       | Indicates registered Mobile Number for the redeem journey                                                |
| CREATED\_BY           | int(11)       | N       | Indicates who created the record. Here -1 indicates administrator created the record                     |
| CREATION\_TIME        | datetime      | N       | Indicates when the record was added in the system                                                        |
| MODIFIED\_BY          | int(11)       | Y       | Indicates who updated the record. Here -1 indicates administrator updated the record                     |
| MODIFIED\_TIME        | datetime      | Y       | Indicates when the record was updated in the system                                                      |

**HOTEL – HTL\_TXN\_PASSENGER\_DETAIL**

| **Column Name**      | **Datatype** | **Description**                                                                                            |
| :------------------- | :----------- | :--------------------------------------------------------------------------------------------------------- |
| booking\_pax\_id     | bigint(20)   | Primary Key of the table                                                                                   |
| hb\_booking\_ref\_no | bigint(20)   | Hold the reference ID for the order created. This would be reference to the table htl\_txn\_hotel\_booking |
| passenger\_no        | int(2)       | Indicates passenger number opted during the booking process                                                |
| is\_lead\_pax        | tinyint(1)   | Indicates passenger is leading pax from Room 1. Here 1 indicates as leading passenger                      |
| room\_seq\_no        | int(2)       | Indicates room sequence number                                                                             |
| title                | varchar(5)   | Indicates Title of the passenger                                                                           |
| first\_name          | varchar(50)  | Indicates First Name of the passenger                                                                      |
| middle\_name         | varchar(50)  | Indicates Middle Name of the passenger                                                                     |
| last\_name           | varchar(50)  | Indicates Last Name of the passenger                                                                       |
| gender               | int(2)       | Indicates gender of the passenger                                                                          |
| dob                  | varchar(20)  | Indicates Date of Birth of the Passenger                                                                   |
| age                  | int(3)       | Indicates age of the Passenger                                                                             |
| email                | varchar(80)  | Indicates Email ID of the Passenger                                                                        |
| country\_code        | varchar(10)  | Indicates country code of the registered mobile number                                                     |
| mobile\_no           | varchar(15)  | Indicates Mobile Number of the passenger                                                                   |
| passanger\_type      | varchar(20)  | Indicates the type of Passenger                                                                            |
| created\_by          | int(11)      | Indicates who created the record. Here -1 indicates administrator created the record                       |
| creation\_time       | datetime     | Indicates when the record was added in the system                                                          |
| modified\_by         | int(11)      | Indicates who updated the record. Here -1 indicates administrator updated the record                       |
| modified\_time       | datetime     | Indicates when the record was updated in the system                                                        |

**Security Aspects**

Data at rest represents any data that you persist in non-volatile storage for any duration in your workload. Protecting your data at rest reduces the risk of unauthorized access, when encryption and appropriate access controls are implemented.

*Data at rest* represents any data that you persist in non-volatile storage for any duration in your workload. This includes block storage, object storage, databases, archives, IoT devices, and any other storage medium on which data is persisted. Protecting your data at rest reduces the risk of unauthorized access, when encryption and appropriate access controls are implemented.

*Encryption* is a way of transforming content in a manner that makes it unreadable without a secret key necessary to decrypt the content back into plaintext.  By defining an encryption approach that includes the storage, rotation, and access control of keys, you can help provide protection for your content against unauthorized users and against unnecessary exposure to authorized users.

AES 256 encryption will be performed at the field level for the key data. The data to be encrypted will be given in a separate annexure.

At the infra level, database encryption will be done.