# PAX Service

This document describes the details regarding the integration of the existing TD GR system and the Vernost system by the PAX Service.

## Why PAX Service is needed ?

The current PAX details/schema which is provided by the Vernost is not adequate to cater to the vison of the TD platform, and its proposed that TD created an extended schema at the TD platform side to cater to capture all the relevant travel specific extension to the existing GR record.

## Entities involved

In travel booking content, there are two entities involved

1. The **Booker** (Customer), who does the actual booking transaction, payment and check out

2. The **PAX**, who is the actual person for which the booking is done (i.e. the traveling person).

The system level login & transaction details will be registered in the name of booker (Customer), while all the loyalty benefits are computed and provided to the actual PAX who travels.

If the customer also becomes a traveler, then a corresponding PAX record (with all travel extension attributes) need to be created for the customer GR record. **Only the PAX records will be made part of any booking records and the associated PAX information details.**

![Booker PAX Relation](images/Booker_PAX_Relationship.png)

## What is PAX Service?

In order **to facilitate the above mentioned travel domain specific extension schema/attributes, its proposed that a new microservice called PAX Service, will be created in the TD Platform side which will provide the necessary API support and data management capabilities associated with the PAX while extending on the existing GR record capabilities**.

It is envisaged to avoid duplication of attributes which are in GR in the PAX schema, however due the specific business flow requirements there may instances where the PAX schema may duplicate certain attributes which are already available in the GR record. If such duplication happens, then the context will decide which attributes to consider, i.e. in a PAX info content, it would be the PAX attributes which will take precedence, while in the GR info context, it would be the GR attributes which will take precedence.

## **Proposed Solution Approach**

1. PAX records exist only in the context of a Customer(Booker) GR record. i.e. for a PAX record to be added to the booking transactional record, the PAX record need to be first created in the GR PAX record table, and obtain the hash/ID. All GR PAX record will always carry the customer hash (GR Hash) reference to which the PAX record is associated with. A relation of the PAX to the customer will also be stored as part of the PAX record.

    A new micro service and APIs end points will be created for this (refer to [**Travel PAX Information Service**](#travel-pax-information-service) )

2. The PAX record can be associated with none, one or more transaction/booking records

3. PAX records will be created and maintained in the TD GR eco system. The PAX IDs/Hash is owned and managed by TD GR PAX system.

4. Same PAX (in content terms) can exist with multiple Customers, however PAX records will be different for each customer.

5. At booking/transactional level, a minimal set of fields (10 to 12) of PAX records will be copied / duplicated at the Vernost platform side as part of the transaction (to maintain conformity to existing PAX info details related to booking transaction info). This is required to manage the business flows at vernost system level.
All such transactional record will also store the GR PAX record hash/ID for cross reference to GR Side for extended attributes stored in GR PAX record.
All other extended attributes of PAX data will be always stored and maintained as part of the GR PAX data table & service. Any access/updates to this part of the PAX should be carried out through the PAX Service & API end points.

6. Any changes / updates / edits carried out to the PAX records at GR side will not impact any PAX info in existing transactional records (ie copied over PAX data). However any new transactions/bookings carried out will take the new updated PAX data.

7. If any changes to be carried out to the transactional level/booking details PAX data, the same need to go through a booking amendment business process.

## Examples

### Example 1

John is an existing Tata Neu app user and he wants to book a ticket for Tony. Here John is Booker and Tony is PAX.
So, a new PAX record will be created only for Tony as he is the one traveling and the details required for the PAX record will be entered by John.
As John is booking for Tony, Tony's PAX record will have some reference to John's GR record as booker id.
While booking, Some of Tony's PAX record essential booking details will be shared with the Vernost system, Which is a booking Engine to do the actual booking.

### Example 2

This Example is an extension of the above example. Now John wants to have one more booking. This time for Tony, Mary, and himself.
So, here John will be the booker as well as the PAX Where Tony and Mary are the only PAX (as all of them are Travelling).
As Tony has already his PAX record created earlier (from previous booking), So a new PAX record will be created only for John and Mary and the details will be entered by John himself (Booker).
As John is the booker, the new PAX record created will have reference to his GR record as booker id.
While booking, Some of their PAX record essential booking details will be shared with the Vernost system, Which is a booking Engine to do the actual booking.

![PAX Example](images/PAX_Example.png)

## **Architecture Approach**

This section describes the overall architecture approach for achieving the proposed integration.

### **ER Diagram**

![ER Diagram](images/ER_Diagram.png)

Note : Each entities which are represented in the above diagram will have its own detailed attributes scheme of their own. The detailed regarding these attributes are limited to the purpose and scope of this integration.

### **Service Integration Architecture**

![Service Integration Architecture](images/Service_Integration_Architecture.png)

### **Data flow Diagram**

High Level DFD

![High Level DFD](images/High_Level_DFD.png)

### **Detailed DFD – Create Passenger**

![Create Passenger DFD](images/Create_Passenger_DFD.png)

### **Integration Call flow**

![Integration Call flow](images/Integration_Call_Flow.png)

Note : to be corrected based on the vernost actual internal call flows for managing PAX records

## Travel PAX Information Service

This will be a new micro service. The details of the APIs and the associated attributes etc are provided below (This needs to be expanded to reflect all the service capabilities which are offered by this service).

API Documentation URL:

<https://tdldev.stoplight.io/studio/travel-pax-profile-service?source=x1tqglvr&symbol=%252Fp%252Freference%252FProfile_PAX_API.json%252Fpaths%252F%7E1pax%7E1%257Bpax_id%257D%7E1info%252Fget>

## **PAX Schema Definition**

Details the travel specific extended attributes associated with the customer/PAX records

![DB Schema](images/DB_Schema.png)

### **Booker Details**

This schema will have extended attributes (travel related) from GR (as PAX Service doesn't have write access into GR)

| **Field Name**   | **Data Type** | **NULL** | **Description**                                  |
| ---------------- | ------------- | -------- | ------------------------------------------------ |
| id (pk)          | UUID          | N        | Unique UUID which identifies the customer record |
| gr_customer_hash | UUID          | N        | not null, gr hash record of the booker           |
| title            | varchar(16)   |          | Indicates title of the customer                  |
| first_name       | varchar(64)   |          | Indicates first name of the customer             |
| last_name        | varchar(64)   |          | Indicates last name of the customer              |
| isdCode          | integer       | N        | Indicates ISD code of the customer               |
| mobile_no        | integer       | N        | Indicates Mobile Number of the customer          |
| email_id         | varchar(64)   | N        | Indicates Email ID of the customer               |

### **PAX Details**

This schema contains details of the PAX (the person who is actually travelling)

| **Field Name**                            | **Data Type** | **NULL** | **Description**                                                                                                    |
| ----------------------------------------- | ------------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| id(PK)                                    | UUID          | N        | Unique UUID which identifies the Pax record                                                                        |
| booker_id(FK)                             | UUID          | N        | If person themselves is passenger then their customer_id will be populated else cutomer_id of booker will be added |
| gr_customer_hash                          | UUID          |          |                                                                                                                    |
| is_booker                                 | Boolean       | N        | boolean field to identify whether the pax is customer or not                                                       |
| title                                     | varchar(16)   | N        | Indicates Title of the passenger                                                                                   |
| first_name                                | varchar(64)   | N        | Indicates First Name of the passenger                                                                              |
| middle_name                               | varchar(64)   | Y        | Indicates Middle Name of the passenger                                                                             |
| last_name                                 | varchar(64)   | N        | Indicates Last Name of the passenger                                                                               |
| name_suffix                               | varchar(16)   |          | name suffix of the person if any                                                                                   |
| nick_name                                 | varchar(64)   |          | Any nick name associated with the PAX                                                                              |
| gender                                    | varchar(16)   |          | Male, Female, Unknown                                                                                              |
| dob                                       | date          | Y        | Indicates Date of Birth of the Passenger                                                                           |
| email_id                                  | varchar(64)   | Y        | Indicates Email ID of the Passenger                                                                                |
| mobile_no                                 | varchar(64)   | Y        | Indicates Mobile Number of the Passenger                                                                           |
| pax_relation                              | varchar(16)   | N        | Indicates the relationship beteween the Customer/Booker and the Pax                                                |
| pax_nationality                           | varchar(16)   | N        |                                                                                                                    |
| disability                                | boolean       |          | Any disability the PAX have                                                                                        |
| special_care                              | varchar(64)   |          | Any special care requirements for the PAX                                                                          |
| home_city                                 | varchar(64)   |          | Home City of the PAX                                                                                               |
| home_airport                              | varchar(64)   |          | Home Airport of the PAX                                                                                            |
| tsa_redress_number                        | varchar(64)   | N        | "TSA = Transportation security administration                                                                      |
| TSA redress number of the PAX if avilable |
| tsa_precheck_number                       | varchar(64)   | N        | TSA precheck number of the PAX if avilable                                                                         |
| local_language_code                       | varchar(16)   |          | A field to identify the Local Language of the PAX                                                                  |
| vip_status                                | Boolean       |          | Defines if the PAX is awarded with any VIP status and details associated                                           |
| pax_classification                        | varchar(16)   |          | The classification assigned to this PAX based on internal classification                                           |
| pax_status                                | varchar(16)   |          | status of this PAX record (active,inactive,merged,archived,etc)                                                    |
| add_to_master                             | boolean       |          | based on flag, making particular traveller to master list                                                          |
| passport_issuing_country                  | varchar(16)   |          | Country of citizenship for this passport is issued                                                                 |
| passport_status                           | varchar(16)   |          | current status of this passport (Valid, Expired)                                                                   |
| passport_no                               | varchar(64)   |          | Passport number                                                                                                    |
| passport_place_of_birth                   | varchar(64)   |          | Place of birth as mentioned in the passport                                                                        |
| passport_issuing_date                     | date          |          | Date of issue of the  passport                                                                                     |
| passport_expiry_date                      | date          |          | Date of expiry of the  passport                                                                                    |

### **PAX Documents**

| **Field Name** | **Data Type** | **NULL** | **Description**                                  |
| -------------- | ------------- | -------- | ------------------------------------------------ |
| pax_id(PK)     | UUID          | N        | Unique UUID which identifies the Pax record      |
| document_id    | UUID          |          | Unique UUID which identifies the document record |

### **Document Details**

This schema will store all the documents of the passenger

| **Field Name**  | **Data Type** | **NULL** | **Description**                                                                                                                                                                |
| --------------- | ------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| id(Pk)          | UUID          | N        | Unique UUID which identifies the document record                                                                                                                               |
| document_type   | varchar(64)   |          | The Travel Document type Passport, Visa , Drivers License , Permanent Residence Card , National Identity Card , Redress Number , Known Traveler Number , Military Card , Other |
| document_URL    | varchar(64)   |          | The URL where which the scanned document can be accesed                                                                                                                        |
| document_number | varchar(64)   |          | The Travel Document number                                                                                                                                                     |
| name            | varchar(16)   |          | Name as specified on the travel document.                                                                                                                                      |
| description     | varchar(64)   |          | Description of the travel document                                                                                                                                             |

### **Flight Preference Details**

This schema contains the flight preferences of the passenger

| **Field Name**              | **Data Type** | **NULL** | **Description**                                                                                                 |
| --------------------------- | ------------- | -------- | --------------------------------------------------------------------------------------------------------------- |
| id(pk)                      | UUID          |          | Unique UUID which identifies the flight preference record                                                       |
| pax_id(FK)                  | UUID          | N        | Referenced from PAX schema                                                                                      |
| purpose                     | varchar(64)   |          | Business , Leisure , Meeting , Group , Incentive , Government , Domestic , International , Conference , Loyalty |
| departure_city_code         | varchar(64)   |          | prefered departure city code (from city look up)                                                                |
| connection_hub_city_code    | varchar(64)   |          | prefered connection hub city especiay for international travels (from city look up)                             |
| connection_hub_airport_code | varchar(64)   |          | prefered connection hub airport especially for international travels (from airport look up)                     |
| fare_type                   | varchar(64)   |          | Advance Purchase Fares, Non-Refundable Fares, Penalty Fares, Private Fares Only, Restricted Fares               |
| seating_section_position    | varchar(64)   |          | Forward, Middle, rear area etc                                                                                  |
| seat_interrow_position      | varchar(64)   |          | Aile, Window, Middle                                                                                            |
| seat_no                     | number        |          | specific seat no pref if any                                                                                    |
| cabin_type                  | varchar(64)   |          | economy, business, first class                                                                                  |
| domestic_preferred_airline  | varchar(64)   |          | prefered flight while travelling in domestic sector                                                             |
| intl_preferred_airline      | varchar(64)   |          | prefered flight while travelling in international sector                                                        |
| meal_type                   | varchar(64)   |          | Prefered meal type Veg,NonVeg,Low Calarorie, etc                                                                |
| max_layover                 | time          |          | maximum layover time acceptable in case of connection flights                                                   |
| itenerary_optimisation      | varchar(64)   |          | optimise the itineray for best / least possible pricing or best / least duration of travel                      |
| max_connections             | number        |          | maximum no of connecting flights prefered                                                                       |
| wheel_chair_assistance      | boolean       |          | specify if wheel chair assistance is required                                                                   |
| general_preference          | varchar(64)   |          | any general prefrences                                                                                          |
| addon_ancillaries           | varchar(64)   |          | Any add on ancillaries always prefered like Meal, baggage, etc                                                  |

### **Hotel Preference Details**

This schema contains the hotel preferences of the passenger

| **Field Name**     | **Data Type** | **NULL** | **Description**                                                                                                 |
| ------------------ | ------------- | -------- | --------------------------------------------------------------------------------------------------------------- |
| id(pk)             | UUID          |          | Unique UUID which identifies the hotel preference record                                                        |
| pax_id(FK)         | UUID          | N        | Referenced from PAX schema                                                                                      |
| purpose            | varchar(64)   |          | Business , Leisure , Meeting , Group , Incentive , Government , Domestic , International , Conference , Loyalty |
| location           | varchar(64)   |          | Location preference, like City Center, Beach Side, etc                                                          |
| chain              | varchar(64)   |          | preferred hotel chain                                                                                           |
| name               | varchar(64)   |          | preferred hotel name                                                                                            |
| bed                | varchar(64)   |          | bed type prefrence ( queen size, king size, etc)                                                                |
| star               | varchar(64)   |          | preferred hotel start rating (1-5 Star)                                                                         |
| rating             | varchar(64)   |          | preferred hotel 3rd party rating                                                                                |
| ameneties          | varchar(64)   |          | Any specific hotel amenity prefered like, Gym, Swimming Pool etc                                                |
| pet_friendly       | boolean       |          | Specifies if a Pet friendly hotel is prefered                                                                   |
| general_preference | varchar(64)   |          | Any other general prefrences                                                                                    |
| room_rate_plan     | varchar(64)   |          | preferred rate plan if any                                                                                      |
| smoking_room       | boolean       |          | requires a smoking or non smoking room                                                                          |
| room_view          | varchar(64)   |          | Any prefrence of room view (Pool View, Garden View, City View,etc)                                              |
| room_floor         | varchar(64)   |          | preferred room floor (ground near, top floors, etc)                                                             |
| room_type          | varchar(64)   |          | preferred room type                                                                                             |
| room_ameneties     | varchar(64)   |          | prefered room amenities if any                                                                                  |
| airport_pickup     | varchar(64)   |          | specify if prefers airport pick up by hotel couch / transport                                                   |
| airport_drop       | varchar(64)   |          | specify if prefers airport drop up by hotel couch / transport                                                   |
| payment_preference | varchar(64)   |          | any payment prefrence the customer has like Pay@Hotel                                                           |

### **Loyalty Membership Details**

This refers to any other loyalty programme membership the customer may be part of other than TATA Nue Pass membership program

| **Field Name**            | **Data Type** | **NULL** | **Description**                                                                                                                                                   |
| ------------------------- | ------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id(PK)                    | UUID          |          |                                                                                                                                                                   |
| pax_id(fk)                | UUID          | N        | Referenced from PAX schema                                                                                                                                        |
| loyalty_product_type      | varchar(64)   |          | The product type with which this loyalty programme is associated with (Air , Vehicle , Hotel , Rail , Other)                                                      |
| loyalty_status            | varchar(64)   |          | The current status of this membership (active, expired, etc)                                                                                                      |
| loyalty_name              | varchar(64)   |          | Loyalty program name such as Frontier-EarlyReturns, Renaissance Intl-Marriott Rewards, Alamo-QuickSilver Service etc. Frequent flier programmes in case of flight |
| loyalty_number            | varchar(16)   |          | A traveler's specific loyalty program number or alphanumeric identifier.                                                                                          |
| loyalty_tier              | varchar(64)   |          | The tier / level associated with the LP such as silver, gold, platinum etc                                                                                        |
| loyalty_expiry_date       | date          |          | The date on which this loyalty programme will expire                                                                                                              |
| loyalty_alliance          | varchar(64)   |          | The alliance if any this loyalty programme belongs to                                                                                                             |
| loyalty_status_benefits   | varchar(64)   |          | Description of a benefit of the loyalty program at the current status. Example: You are entitled to free breakfast                                                |
| loyalty_points_total      | number        |          | The customers’s total number of points in the loyalty program.                                                                                                    |
| loyalty_segment_total     | varchar(64)   |          | The user’s total  segments in the loyalty program.                                                                                                                |
| loyalty_next_tier         | varchar(64)   |          | Next tier of the loyalty programme which the customer can get upgrade to.                                                                                         |
| loyalty_points_next_tier  | number        |          | Loyalty points required to get upgraded to next tier level                                                                                                        |
| loyalty_segment_next_tier | varchar(64)   |          | Booking segments to next tier level                                                                                                                               |

## **Security Aspects**

Data at rest represents any data that you persist in non-volatile storage for any duration in your workload. Protecting your data at rest reduces the risk of unauthorized access, when encryption and appropriate access controls are implemented.

*Data at rest* represents any data that you persist in non-volatile storage for any duration in your workload. This includes block storage, object storage, databases, archives, IoT devices, and any other storage medium on which data is persisted. Protecting your data at rest reduces the risk of unauthorized access, when encryption and appropriate access controls are implemented.

*Encryption* is a way of transforming content in a manner that makes it unreadable without a secret key necessary to decrypt the content back into plaintext.  By defining an encryption approach that includes the storage, rotation, and access control of keys, you can help provide protection for your content against unauthorized users and against unnecessary exposure to authorized users.

AES 256 encryption will be performed at the field level for the key data. The data to be encrypted will be given in a separate annexure.

At the infra level, database encryption will be done.
